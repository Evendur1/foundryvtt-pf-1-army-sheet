import { NoteEditor } from "../apps/note-editor.js";
import { SettingsEditor } from "../apps/settings.js";
import { skillAlwaysShown, getSkipActionPrompt } from "./helpers.js";
import { ActorSheetPFCharacter, ActorSheetPFNPC } from "../../../systems/pf1/pf1.js";

/*
In a "perfect" world, I would use a mixin here:
const AltSheetMixin = (BaseClass) => class extends BaseClass { ... };
class AltActorSheetPFCharacter extends AltSheetMixin(ActorSheetPFCharacter) {};
class AltActorSheetPFNPC extends AltSheetMixin(ActorSheetPFNPC) {};

That way, we would have no code duplication here.
And no Object.assign();
However, this breaks event bubbling to the base classes BUT ONLY IN BROWSERS!
Desktop electron foundry is fine.

Revisit this in a year or whenever.
*/
export class AltActorSheetPFCharacter extends ActorSheetPFCharacter {
    get template() {
        // TODO: change this to our own lite sheet
        if ( !game.user.isGM && this.actor.limited ) return "systems/pf1/templates/actors/limited-sheet.hbs";
        return "modules/pf1-alt-sheet/templates/altsheet.hbs";
    }

    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["pf1alt", "sheet", "actor", "character"],
            width: 820,
            height: 840,
            scrollY: [
                ".buffs-body",
                ".feats-body",
                ".spells_primary-body",
                ".spells_secondary-body",
                ".spells_tertiary-body",
                ".spells_spelllike-body",
                ".skills-list.adventure",
                ".skills-list.background",
                ".combat-attacks",
                ".inventory-body",
                ".attributes-body"
            ],
            tabs: [
                {
                    navSelector: "nav.tabs[data-group='primary']",
                    contentSelector: "section.primary-body",
                    initial: "summary",
                },
                {
                    navSelector: "nav.tabs[data-group='skillset']",
                    contentSelector: "section.skillset-body",
                    initial: "adventure",
                },
                {
                    navSelector: "nav.tabs[data-group='spellbooks']",
                    contentSelector: "section.spellbooks-body",
                    initial: "primary",
                },
            ]
        });
    }

    get currentPrimaryTab() {
        const primaryElem = this.element.find('nav[data-group="primary"] .item.active');
        if (primaryElem.length !== 1) return null;
        return primaryElem.attr("data-tab");
    }

    async getData()
    {
        return this.mixinGetData(await super.getData());
    }

    activateListeners(html)
    {
        super.activateListeners(html);
        this.mixinActivateListeners(html);
    }
};

// Yes, code duplication. Because JS.
export class AltActorSheetPFNPC extends ActorSheetPFNPC {
    get template() {
        // TODO: change this to our own lite sheet
        if ( !game.user.isGM && this.actor.limited ) return "systems/pf1/templates/actors/limited-sheet.hbs";
        return "modules/pf1-alt-sheet/templates/altsheet.hbs";
    }

    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["pf1alt", "sheet", "actor", "character"],
            width: 820,
            height: 840,
            scrollY: [
                ".buffs-body",
                ".feats-body",
                ".spells_primary-body",
                ".spells_secondary-body",
                ".spells_tertiary-body",
                ".spells_spelllike-body",
                ".skills-list.adventure",
                ".skills-list.background",
                ".combat-attacks",
                ".inventory-body",
                ".attributes-body"
            ],
            tabs: [
                {
                    navSelector: "nav.tabs[data-group='primary']",
                    contentSelector: "section.primary-body",
                    initial: "summary",
                },
                {
                    navSelector: "nav.tabs[data-group='skillset']",
                    contentSelector: "section.skillset-body",
                    initial: "adventure",
                },
                {
                    navSelector: "nav.tabs[data-group='spellbooks']",
                    contentSelector: "section.spellbooks-body",
                    initial: "primary",
                },
            ]
        });
    }

    get currentPrimaryTab() {
        const primaryElem = this.element.find('nav[data-group="primary"] .item.active');
        if (primaryElem.length !== 1) return null;
        return primaryElem.attr("data-tab");
    }

    async getData()
    {
        return await this.mixinGetData(await super.getData());
    }

    activateListeners(html)
    {
        super.activateListeners(html);
        this.mixinActivateListeners(html);
    }
};

// Define shared functions here.
// DO NOT USE super, IT DOES NOT WORK AND I CAN'T USE A MIXIN CLASS BECAUSE REASONS!
export const AltSheetMixin = {
    defaultConfig()
    {
        return {
            skills: {
            },
            features: {
                minimizeClasses: false,
            },
            links: {
                mode: "none",
            }
        }
    },

    getModuleActorConfig()
    {
        // Check if there is a config for this actor
        let config = this.actor.getFlag("pf1-alt-sheet", "config");
        if (config === undefined)
        {
            config = this.defaultConfig();
        }
        return config;
    },

    resetModuleActorConfig()
    {
        console.log("pf1-alt-sheet | Resetting actor config");
        this.actor.unsetFlag("pf1-alt-sheet", "config");
    },

    setModuleActorConfig(update)
    {
        const data = mergeObject(this.getModuleActorConfig(), update, { inplace: false });
        this.actor.setFlag("pf1-alt-sheet", "config", data);
    },

    setLinkOptions(options)
    {
        this.setModuleActorConfig({
            links: {
                mode: options.mode
            }
        });
    },

    async mixinGetData(data) {
        let classesHidden = !data.filters.features.has("type-classes");
        if (data.filters.features.size == 0)
        {
            classesHidden = false;
        }

        let al =  game.modules.get("koboldworks-pf1-actor-link");
        const actorLinkEnabled = al && al.active;

        // inject some stuff for our sheet
        data = mergeObject(data, {
            attacks: {
                weapon: {
                    canBreak: true,
                },
            },
            system: {
                abilities: {
                    str: { labelShort: game.i18n.localize("PF1.AbilityShortStr") },
                    dex: { labelShort: game.i18n.localize("PF1.AbilityShortDex") },
                    con: { labelShort: game.i18n.localize("PF1.AbilityShortCon") },
                    int: { labelShort: game.i18n.localize("PF1.AbilityShortInt") },
                    wis: { labelShort: game.i18n.localize("PF1.AbilityShortWis") },
                    cha: { labelShort: game.i18n.localize("PF1.AbilityShortCha") },
                },
                attributes: {
                    ac: {
                        normal: { labelShort: game.i18n.localize("PF1AS.ACShort") },
                        touch: { labelShort: game.i18n.localize("PF1AS.TouchShort") },
                        flatFooted: { labelShort: game.i18n.localize("PF1AS.FFShort") }
                    },
                }
            },
            skillsets: {
                known: { skills: {} }
            },
            inventory: {
                weapon: { canBreak: true },
                equipment: { canBreak: true },
            },
            config: {
                conditionCompendiumEntries: {
                    bleed: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.L6DTocj1PbOtuspU",
                    pf1_blind: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.A9KUpd2bsdZZsQqj",
                    confused: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.J2yma0xciBKRUh9t",
                    cowering: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.eNW5geiJqrnAjjNu",
                    dazed: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.2A6Gk60pLDKR2zT0",
                    dazzled: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.xHUnCadQ2qYsfvV0",
                    pf1_deaf: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.3uIamlB0L1UZUwoF",
                    entangled: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.oxmugd8SoxVjvRRl",
                    exhausted: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.ITxxh53qgl74wWcE",
                    fatigued: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.AVaxTNildXRzYnQm",
                    frightened: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.uqpJ7ZMnWF5qjOTl",
                    grappled: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.i4gHNAVajJWs4cFI",
                    helpless: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.6dtCytJsNkO8Hwq4",
                    incorporeal: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.acqGBiMxTbXZ47zU",
                    invisible: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.Wr2ZWUZcyVWQ4GtX",
                    nauseated: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.ySiyyK1BMAyKPY4I",
                    panicked: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.nGTsTfpWcJTTU3rk",
                    paralyzed: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.24p2Koq4FFCLDKnj",
                    pinned: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.poCq8gXdDi5leaMT",
                    pf1_prone: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.VbagnIPQS523dwxa",
                    shaken: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.XxLUswkCuXnXmA5T",
                    sickened: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.7LwCMwrX3tchvrjW",
                    pf1_sleep: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.6dtCytJsNkO8Hwq4", // same as helpless
                    staggered: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.TTp8q9Vb2PNAujWu",
                    stunned: "Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.ZgsBPY0uVCVl6SGh"
                }
            },
            altSheet: this.getModuleActorConfig(),
            actorLinkEnabled: actorLinkEnabled,
        }, { inplace: false });

        // add hpMax to each class
        for (let clazz of data.classes)
        {
            clazz.hpMax = clazz.hd * clazz.level;
        }

        // Inject classes so that the filters include classes for the features
        data.features = {
            classes: {
                label: game.i18n.localize("PF1.Classes"),
                items: data.classes,
                isClass: true,
                canCreate: data.features.feat.canCreate,
                hasActions: false,
                dataset: { type: "class", "feat-type": "class"},
                _hidden: classesHidden,
            },
            ...data.features
        };

        // build the "known skill" list for the first page of the sheet, taken from 3.5e
        let skillkeys = Object.keys(data.skillsets.all.skills).sort(function(a,b) {
            if (data.skillsets.all.skills[a].custom && !data.skillsets.all.skills[b].custom) return 1;
            if (!data.skillsets.all.skills[a].custom && data.skillsets.all.skills[b].custom) return -1;
            return ('' + data.skillsets.all.skills[a].label).localeCompare(data.skillsets.all.skills[b].label)
        });
        skillkeys.forEach( a => {
            let skl = data.skillsets.all.skills[a]
            if (skl.rank > 0 || skillAlwaysShown(data, a, undefined))
            {
                data.skillsets.known.skills[a] = skl;
            }
            else if (skl.subSkills !== undefined)
            {
                data.skillsets.known.skills[a] = skl;
            }
        })

        // actor link familiar mode for skills
        if (actorLinkEnabled)
        {
            data = await this.doActorLinkStuff(data);
        }

        // add some things to the spellbooks
        const books = ["primary", "secondary", "tertiary", "spelllike"];
        for (const book of books)
        {
            if (data.spellbookData[book].orig.inUse)
            {
                const abl = data.spellbookData[book].orig.ability;
                for (const [level, _] of Object.entries(data.spellbookData[book].data))
                {
                    // We fake the ablScore to 9 if none is set so that all levels can be casted.
                    const ablScore = abl === "" ? 9 : data.system.abilities[abl].total - 10;
                    const spellLevel = data.spellbookData[book].data[level].level;
                    const cannotCastAblScore =  !(spellLevel <= ablScore);
                    data.spellbookData[book].data[level].canCast = {
                        value: !cannotCastAblScore,
                        cannotCastAblScore: cannotCastAblScore,
                    };
                }
            }
        }

        //console.log(data);

        this.actorData = data;

        return data;
    },

    async doActorLinkStuff(data)
    {
        // Check if this actor is linked to another
        let linked = data.data._linked;
        if (!linked)
        {
            return data;
        }
        let modSkillsets = {
            skillsets: {
                all: {
                    skills: {}
                }
            }
        }
        // Check if mode is familiar
        if (data.altSheet.links.mode === "familiar")
        {
            // Inject master ranks into skills
            for (let [name, skill] of Object.entries(data.skillsets.all.skills))
            {
                modSkillsets.skillsets.all.skills[name] = {};
                modSkillsets.skillsets.all.skills[name].masterRank = linked.skills[name].rank;
                let clb = ((skill.rank ?? 0) == 0 && linked.skills[name].rank > 0 && skill.cs) ? 3 : 0;
                modSkillsets.skillsets.all.skills[name].masterMod = skill.mod - (skill.rank ?? 0) + (linked.skills[name].rank ?? 0) + clb;
                if (!(data.skillsets.all.skills[name].subSkills))
                {
                    continue;
                }
                for (let [subName, subSkill] of Object.entries(data.skillsets.all.skills[name].subSkills))
                {
                    if (subName in linked.skills[name].subSkills)
                    {
                        if (!('subSkills' in modSkillsets.skillsets.all.skills[name]))
                        {
                            modSkillsets.skillsets.all.skills[name].subSkills = {};
                        }
                        modSkillsets.skillsets.all.skills[name].subSkills[subName] = {};
                        modSkillsets.skillsets.all.skills[name].subSkills[subName].masterRank = linked.skills[name].subSkills[subName].rank;
                        clb = ((subSkill.rank ?? 0) == 0 && linked.skills[name].subSkills[subName].rank > 0 && skill.cs) ? 3 : 0
                        modSkillsets.skillsets.all.skills[name].subSkills[subName].masterMod = subSkill.mod - (subSkill.rank ?? 0) + (linked.skills[name].subSkills[subName].rank ?? 0) + clb;
                    }
                }
            }
            data = mergeObject(data, modSkillsets);
        }

        return data;
    },

    changesMod(sourceArr){
        let total = 0;
        sourceArr.forEach(c => {
            total += c.value;
        });
        return total;
    },

    mixinActivateListeners(html) {
        html.find(".note-editor").click(this._onNoteEditor.bind(this));

        html.find(".settings-button").click(this._onSettings.bind(this));

        html.find(".spell-slots-per-level span.text-box").on("click", (event) => {
            this._onSpanTextInput(event, this._onSubmit.bind(this));
        });

        html.find(".item .item-name .class-item-name").contextmenu(this._onItemEdit.bind(this));

        html.find(".skill > .skill-mod-total > .rollable").click(this._onRollSkillCheck.bind(this));
        html.find(".sub-skill > .skill-mod-total > .rollable").click(this._onRollSubSkillCheck.bind(this));

        html.find(".skill > .skill-name > .rollable").click(this._onRollSkillCheck.bind(this));
        html.find(".sub-skill > .skill-name > .rollable").click(this._onRollSubSkillCheck.bind(this));

        html.find(".skill > .skill-master-mod-total > .rollable").click(this._onRollMasterSkillCheck.bind(this));
        html.find(".sub-skill > .skill-master-mod-total > .rollable").click(this._onRollMasterSubSkillCheck.bind(this));

        html.find(".altsheet-skill-checkbox").click(this._onAltSheetCheckbox.bind(this));
        html.find(".altsheet-minimize-classes-checkbox").click(this._onAltSheetCheckbox.bind(this));

        html.find(".spellcasting-concentration .rollable").click(this._onRollConcentration.bind(this));
        html.find(".spellcasting-cl .rollable").click(this._onRollCL.bind(this));
    },

    _onAltSheetCheckbox(event) {
        event.preventDefault();
        event.stopPropagation();
        const path = event.target.attributes["data-altsheet-config-path"].value;
        const currentValue = getProperty(this.getModuleActorConfig(), path);
        let newData = {};
        setProperty(newData, path, !currentValue);
        this.setModuleActorConfig(newData);
    },

    _onNoteEditor(event) {
        event.preventDefault();
        const a = event.currentTarget;
        const options = {
            name: a.getAttribute("for"),
            title: a.getAttribute("title"),
            fields: a.dataset.fields,
            dtypes: a.dataset.dtypes,
        };
        new NoteEditor(this.actor, options).render(true);
    },

    _onSettings(event) {
        event.preventDefault();
        const a = event.currentTarget;
        const options = {
            name: "settings",
            title: game.i18n.localize("PF1.Settings"),
            fields: a.dataset.fields,
            dtypes: a.dataset.dtypes,
        };
        new SettingsEditor(this, options).render(true);
    },

    _onRollMasterSkillCheck(event) {
        event.preventDefault();
        const skill = event.currentTarget.parentElement.parentElement.dataset.skill;
        let originalRanks = this.document.data.data.skills[skill].rank;
        this.document.data.data.skills[skill].rank = this.document.data.data._linked.skills[skill].rank;
        this.document.rollSkill(skill, { event: event, skipDialog: getSkipActionPrompt() });
        this.document.data.data.skills[skill].rank = originalRanks;
    },

    _onRollMasterSubSkillCheck(event) {
        event.preventDefault();
        const mainSkill = event.currentTarget.parentElement.parentElement.dataset.mainSkill;
        const skill = event.currentTarget.parentElement.parentElement.dataset.skill;
        let originalRanks = this.document.data.data.skills[mainSkill].subSkills[skill].rank;
        this.document.data.data.skills[mainSkill].subSkills[skill].rank = this.document.data.data._linked.skills[mainSkill].subSkills[skill].rank;
        this.rollSkill(`${mainSkill}.subSkills.${skill}`, { event: event, skipDialog: getSkipActionPrompt() });
        this.document.data.data.skills[mainSkill].subSkills[skill].rank = originalRanks;
    },
}
