export class SettingsEditor extends FormApplication {
    constructor(...args) {
        super(...args);

        //this.noteData = duplicate(getProperty(this.object.data, this.attribute) || "");
        this.sheet = this.object;
        //console.log(this.object);
    }

    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            id: "settings-editor",
            classes: ["pf1alt", "entry"],
            title: game.i18n.localize("PF1.Settings"),
            template: "modules/pf1-alt-sheet/templates/apps/settings.hbs",
            width: 600,
            height: 600,
            closeOnSubmit: false,
            submitOnClose: false,
        });
    }

    get attribute() {
        return this.options.name;
    }

    getData() {
        return this.sheet.actorData;
    }

    activateListeners(html) {
        html.find('button[type="submit"]').click(this._submitAndClose.bind(this));

        //html.find('textarea').change(this._onEntryChange.bind(this));
    }

    async _onEntryChange(event) {
        const a = event.currentTarget;
        this.noteData = a.value;
    }

    async _updateObject(event, formData) {
        let updateData = {
            system: {
                attributes: {
                    spells: this.sheet.actor.system.attributes.spells,
                    hpAbility: this.sheet.actor.system.attributes.hpAbility,
                    cmbAbility: this.sheet.actor.system.attributes.cmbAbility,
                    init: {
                        ability: this.sheet.actor.system.attributes.init.ability,
                    },
                    attack: {
                        meleeAbility: this.sheet.actor.system.attributes.attack.meleeAbility,
                        rangedAbility: this.sheet.actor.system.attributes.attack.rangedAbility
                    },
                    woundThresholds: {
                        override: this.sheet.actor.system.attributes.woundThresholds.override
                    },
                    ac: {
                        normal: { ability: this.sheet.actor.system.attributes.ac.normal.ability },
                        touch: { ability: this.sheet.actor.system.attributes.ac.touch.ability }
                    },
                    cmd: {
                        strAbility: this.sheet.actor.system.attributes.cmd.strAbility,
                        dexAbility: this.sheet.actor.system.attributes.cmd.dexAbility,
                    },
                    savingThrows: {
                        fort: { ability: this.sheet.actor.system.attributes.savingThrows.fort.ability },
                        ref: { ability: this.sheet.actor.system.attributes.savingThrows.ref.ability },
                        will: { ability: this.sheet.actor.system.attributes.savingThrows.will.ability },
                    }
                },
                details: {
                    tooltip: {
                        name: this.sheet.actor.system.details.tooltip.name,
                        hideHeld: this.sheet.actor.system.details.tooltip.hideHeld,
                        hideArmor: this.sheet.actor.system.details.tooltip.hideArmor,
                        hideBuffs: this.sheet.actor.system.details.tooltip.hideBuffs,
                        hideConditions: this.sheet.actor.system.details.tooltip.hideConditions,
                        hideClothing: this.sheet.actor.system.details.tooltip.hideClothing,
                        hideName: this.sheet.actor.system.details.tooltip.hideName,
                    }
                }
            }
        };

        /*
        console.log(event);
        console.log(formData);
        */

        if (event.target.name === "reset-config")
        {
            // Delete altsheet config.
            this.sheet.resetModuleActorConfig();
        }

        const linkOptions = {
            mode: formData["altSheet.links.mode"]
        };
        this.sheet.setLinkOptions(linkOptions);

        for (let [sk, spellbook] of Object.entries(this.sheet.actor.system.attributes.spells.spellbooks))
        {
            updateData.system.attributes.spells.spellbooks[sk].inUse = formData["spellbook-" + sk + "-inuse"]
        }

        updateData.system.attributes.hpAbility = formData.hpAbility;
        updateData.system.attributes.cmbAbility = formData.cmbAbility;
        updateData.system.attributes.init.ability = formData["system.attributes.init.ability"];
        updateData.system.attributes.attack.meleeAbility = formData.meleeAbility;
        updateData.system.attributes.attack.rangedAbility = formData.rangedAbility;
        updateData.system.attributes.woundThresholds.override = formData["system.attributes.woundThresholds.override"];
        updateData.system.attributes.ac.touch.ability = formData["system.attributes.ac.touch.ability"];
        updateData.system.attributes.ac.normal.ability = formData["system.attributes.ac.normal.ability"];
        updateData.system.attributes.cmd.strAbility = formData["system.attributes.cmd.strAbility"];
        updateData.system.attributes.cmd.dexAbility = formData["system.attributes.cmd.dexAbility"];
        updateData.system.attributes.savingThrows.fort.ability = formData["system.attributes.savingThrows.fort.ability"];
        updateData.system.attributes.savingThrows.ref.ability = formData["system.attributes.savingThrows.ref.ability"];
        updateData.system.attributes.savingThrows.will.ability = formData["system.attributes.savingThrows.will.ability"];
        updateData.system.details.tooltip.name = formData["system.details.tooltip.name"];
        updateData.system.details.tooltip.hideHeld = formData["system.details.tooltip.hideHeld"];
        updateData.system.details.tooltip.hideArmor = formData["system.details.tooltip.hideArmor"];
        updateData.system.details.tooltip.hideBuffs = formData["system.details.tooltip.hideBuffs"];
        updateData.system.details.tooltip.hideConditions = formData["system.details.tooltip.hideConditions"];
        updateData.system.details.tooltip.hideClothing = formData["system.details.tooltip.hideClothing"];
        updateData.system.details.tooltip.hideName = formData["system.details.tooltip.hideName"];

        return this.sheet.actor.update(updateData);
    }

    async _submitAndClose(event) {
        event.preventDefault();
        await this._onSubmit(event);
        this.close();
    }
}
